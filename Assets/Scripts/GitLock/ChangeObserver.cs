﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GitLock
{
    [InitializeOnLoad]
    public static class ChangeMonitor
    {
        static ChangeMonitor()
        {
            PrefabStage.prefabStageOpened += OnPrefabStageOpened;
            EditorSceneManager.sceneDirtied += OnSceneDirty;
        }

        private static void OnSceneDirty(Scene scene)
        {
            
            Debug.LogError("Locked scene change: " + scene.name);
        }

        private static void OnPrefabStageOpened(PrefabStage obj)
        {
            Debug.LogError("Opening locked prefab " + obj.prefabContentsRoot.name);
        }

        private static void CheckGitLFSLock()
        {
            LockedPopup.Init();
        }
    }
}
#endif
